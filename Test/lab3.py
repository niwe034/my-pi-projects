import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
import tornado.websocket
import RPi.GPIO as GPIO
import time
Relay = 17
LED = 27
Buzzer = 13

from tornado.options import define, options

define("port", default=8080, help="run on the given port", type=int)
clients = []

class IndexHandler(tornado.web.RequestHandler):
    def get(self):
        self.render('index.html')

class JqueryHandler(tornado.web.RequestHandler):
    def get(self):
        self.render('jquery.min.js')
 
class WebSocketHandler(tornado.websocket.WebSocketHandler):
    def open(self):
        clients.append(self)
        print 'new connection'
        self.write_message("connected")
 
    def on_message(self, message):
        print 'message received %s' % message
        self.write_message('message received %s' % message)
	if message ==  "1" :
           GPIO.output(Relay, True)
           GPIO.output(LED, False)
           GPIO.output(Buzzer, True)
           time.sleep(0.05)
           GPIO.output(Buzzer, False)
           for c in clients :
               c.write_message("1")
        else :
           GPIO.output(LED, True)
           GPIO.output(Relay, False)
           GPIO.output(Buzzer, True)           
           time.sleep(0.05)
           GPIO.output(Buzzer, False)
           time.sleep(0.05)
           GPIO.output(Buzzer,True)
           time.sleep(0.05)
           GPIO.output(Buzzer, False)
                      
           for c in clients :
               c.write_message("0")
 
    def on_close(self):
        clients.remove(self)
        print 'connection closed'
 
if __name__ == "__main__":
    try:
        tornado.options.parse_command_line()
        app = tornado.web.Application(
            handlers=[
                (r"/", IndexHandler),
                (r"/jquery\.min\.js", JqueryHandler),
                (r"/ws", WebSocketHandler)
            ]
        )
        httpServer = tornado.httpserver.HTTPServer(app)
        httpServer.listen(options.port)
        GPIO.setmode(GPIO.BCM)
        
        GPIO.setup(Relay, GPIO.OUT)
        GPIO.setup(LED, GPIO.OUT)
        GPIO.setup(Buzzer, GPIO.OUT)
        
        print "Listening on port:", options.port
        tornado.ioloop.IOLoop.instance().start()
    except KeyboardInterrupt:
        GPIO.output(LED , True)
        GPIO.output(Buzzer , False)
        GPIO.output(Relay , False)
        print "Program Alerady Closed."        
        pass
    finally:        
        GPIO.cleanup()
