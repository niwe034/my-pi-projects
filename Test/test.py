import RPi.GPIO as GPIO
import time
GPIO.setmode(GPIO.BCM)

Relay = 17
LED = 27
Buzzer = 13

GPIO.setup(Relay, GPIO.OUT)
GPIO.setup(LED, GPIO.OUT)
GPIO.setup(Buzzer, GPIO.OUT)

GPIO.output(Relay, True)
GPIO.output(LED, True)
GPIO.output(Buzzer, True)

GPIO.output(Buzzer, False)
GPIO.output(Relay, False)
GPIO.output(LED, False)

GPIO.cleanup()